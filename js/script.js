
const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelectorAll('.tab-content');

tabs.addEventListener('click', ev => {
    const prevActiveTab = document.querySelector('.tabs-title.active');
    prevActiveTab.classList.remove('active');
    ev.target.classList.add('active');
    const prevActiveTabContent = document.querySelector('.tab-content.active');
    prevActiveTabContent.classList.remove('active');
    const activeTab = ev.target.getAttribute('data-name');
    const activeTabContent = document.querySelector(`.tab-content[data-name=${activeTab}]`);
    activeTabContent.classList.add('active');
})
